'''
    File name: https-unpinner.py
    Author: Borja Molina (https://gitlab.com/serralba)
    Date created: 11/2/2019
    Version: 1.0.1
    Utility used to bypass certificate pinning of Android devices. It hooks pinning functions of Android internals and common libraries, like okhttp3, using frida.
'''


import frida
import sys

def help ():
	print "https-unpinner.py automatically avoids the check of certificates of an app"
	print ""
	print ""
	print "\tUsage: https-unpinner.py APP-PACKAGE"
	print ""


def on_message (message, data):
    if message['type'] == 'send':
        print("[*] {0}".format(message['payload']))
    else:
        print(message)



'''
'''

jscode = """
Java.perform(function () {

	/*******
    Android internals
    *******/

    console.log('[+] Injecting code on Android internals');
    
    var and_internal_okhttp = Java.use('com.android.okhttp.internal.tls.OkHostnameVerifier');
    
    and_internal_okhttp.verify.overload('java.lang.String', 'javax.net.ssl.SSLSession').implementation = function (hostname, session) {
		console.log('[!] Android internal okhttp.verify(String, SSLSession) called');
		if (hostname!=null){
			console.log('Checking: '+hostname);
		}
		var	ret = this.verify(hostname, session);
		console.log('	Original verify() call returned: '+ret);
		return true;
	};
	
	and_internal_okhttp.verify.overload('java.lang.String', 'java.security.cert.X509Certificate').implementation = function (hostname, cert) {
		console.log('[!] Android internal okhttp.verify(String, X509) called');
		if (hostname!=null){
			console.log('Checking: '+hostname);
		}
		var	ret = this.verify(hostname, cert);
		console.log('	Original verify() call returned: '+ret);
		return true;
	};
	
	console.log('	[+] Android internal okhttp.verify() implementations intercepted');
	
	
	var netinfo = Java.use('android.net.NetworkInfo');
	
	netinfo.isConnected.overload.implementation = function () {
		console.log('[!] ConnectivityManager NetworkInfo.isConnected() called');
		var ret = this.isConnected();
		console.log('	Original isConnected() call returned: '+ret);
		return true;
	};
	
	console.log('	[+] Android NetworkInfo.isConnected() intercepted');
	
	/*********
	android/net/http/CertificateChainValidator
	*********/
	
	var certchainval = Java.use('android.net.http.CertificateChainValidator');
	
	certchainval.verifyServerDomainAndCertificates.overload('[Ljava.security.cert.X509Certificate;','java.lang.String', 'java.lang.String').implementation = function (certs, domain, authType) {
		console.log('[!] CertificateChainValidator.verifyServerDomainCertificates() called');
		console.log('	Verifying: '+domain);
		console.log('	AuthType: '+authType);
		return null;
	};
	
	console.log('	[+] Android CertificateChainValidator class intercepted');
	
	
	
	/****
	https://android.googlesource.com/platform/external/conscrypt/+/idea133-weekly-release/src/main/java/org/conscrypt/TrustManagerImpl.java
	****/
	
	console.log('[+] Injecting code on Android TrustManager implementations');	
	
	var andtrust = Java.use('com.android.org.conscrypt.TrustManagerImpl');
	
	
	andtrust.checkServerTrusted.overload('[Ljava.security.cert.X509Certificate;','java.lang.String','javax.net.ssl.SSLEngine').implementation = function (chain, authType, engine) {
   		console.log('[!] TrustManagerImpl.checkServerTrusted([LX509, String, SSLEngine) called');
   		console.log('	Arguments: '+authType+', '+engine);
   	};
   	
   	andtrust.checkServerTrusted.overload('[Ljava.security.cert.X509Certificate;','java.lang.String','java.lang.String').implementation = function (chain, authType, hostname) {
   		console.log('[!] TrustManagerImpl.checkServerTrusted([LX509, String, String) called');
   		console.log('	Checking: '+authType+' for '+hostname);
   		var X509Certificate = Java.use('[Ljava.security.cert.X509Certificate');
   		return X509Certificate.$new();
   	};
   	
   	andtrust.checkServerTrusted.overload('[Ljava.security.cert.X509Certificate;', 'java.lang.String').implementation = function (chain, str) {
   		console.log('[!] TrustManagerImpl.checkServerTrusted([LX509, String) called');
   		console.log('	Arguments: '+str);
   	};
   	
   	andtrust.checkClientTrusted.overload('[Ljava.security.cert.X509Certificate;','java.lang.String','javax.net.ssl.SSLEngine').implementation = function (chain, authType, engine) {
   		console.log('[!] TrustManagerImpl.checkClientTrusted([LX509, String, SSLEngine) called');
   		console.log('	Arguments: '+authType+', '+engine);
   	};
   	
   	andtrust.checkServerTrusted.overload('[Ljava.security.cert.X509Certificate;','java.lang.String','java.net.Socket').implementation = function (chain, authType, engine) {
   		console.log('[!] TrustManagerImpl.checkServerTrusted([LX509, String, Socket) called');
   		console.log('	Arguments: '+authType+', '+engine);
   	};
   	
   	andtrust.checkClientTrusted.overload('[Ljava.security.cert.X509Certificate;','java.lang.String','java.net.Socket').implementation = function (chain, authType, engine) {
   		console.log('[!] TrustManagerImpl.checkClientTrusted([LX509, String, Socket) called');
   		console.log('	Arguments: '+authType+', '+engine);
   	};
   	
   	andtrust.getAcceptedIssuers.implementation = function () {
		console.log('[!] TrustManagerImpl.getAcceptedIssuers() called');
		return null;
	};
	
	console.log('	[+] Android internal conscrypt.TrustManagerImpl class instances intercepted');
	
	
	/***
	android/security/net/config/RootTrustManager
	***/
	
	var roottrust = Java.use('android.security.net.config.RootTrustManager');
	
	roottrust.checkServerTrusted.overload('[Ljava.security.cert.X509Certificate;','java.lang.String','javax.net.ssl.SSLEngine').implementation = function (chain, authType, engine) {
   		console.log('[!] RootTrustManager.checkServerTrusted([LX509, String, SSLEngine) called');
   		console.log('	Arguments: '+authType+', '+engine);
   	};
   	
   	roottrust.checkServerTrusted.overload('[Ljava.security.cert.X509Certificate;','java.lang.String','java.lang.String').implementation = function (chain, authType, hostname) {
   		console.log('[!] RootTrustManager.checkServerTrusted([LX509, String, String) called');
   		console.log('	Checking: '+authType+' for '+hostname);
   		var X509Certificate = Java.use('[Ljava.security.cert.X509Certificate');
   		return X509Certificate.$new();
   	};
   	
   	roottrust.checkServerTrusted.overload('[Ljava.security.cert.X509Certificate;', 'java.lang.String').implementation = function (chain, str) {
   		console.log('[!] RootTrustManager.checkServerTrusted([LX509, String) called');
   		console.log('	Arguments: '+str);
   	};
   	
   	roottrust.checkClientTrusted.overload('[Ljava.security.cert.X509Certificate;','java.lang.String','javax.net.ssl.SSLEngine').implementation = function (chain, authType, engine) {
   		console.log('[!] RootTrustManager.checkClientTrusted([LX509, String, SSLEngine) called');
   		console.log('	Arguments: '+authType+', '+engine);
   	};
   	
   	roottrust.checkServerTrusted.overload('[Ljava.security.cert.X509Certificate;','java.lang.String','java.net.Socket').implementation = function (chain, authType, engine) {
   		console.log('[!] RootTrustManager.checkServerTrusted([LX509, String, Socket) called');
   		console.log('	Arguments: '+authType+', '+engine);
   	};
   	
   	roottrust.checkClientTrusted.overload('[Ljava.security.cert.X509Certificate;','java.lang.String','java.net.Socket').implementation = function (chain, authType, engine) {
   		console.log('[!] RootTrustManager.checkClientTrusted([LX509, String, Socket) called');
   		console.log('	Arguments: '+authType+', '+engine);
   	};
   	
	roottrust.getAcceptedIssuers.implementation = function () {
		console.log('[!] RootTrustManager.getAcceptedIssuers() called');
		return null;
	};
	
	console.log('	[+] Android internal RootTrustManager class instances intercepted');
	
	
	/***
	android/security/net/config/NetworkSecurityTrustManager
	***/
	
	var netsectrust = Java.use('android.security.net.config.NetworkSecurityTrustManager');
	
	netsectrust.getAcceptedIssuers.implementation = function () {
		console.log('[!] NetworkSecurityTrustManager.getAcceptedIssuers() called');
		return null;
	};
	
	netsectrust.checkServerTrusted.overload('[Ljava.security.cert.X509Certificate;','java.lang.String','javax.net.ssl.SSLEngine').implementation = function (chain, authType, engine) {
   		console.log('[!] NetworkSecurityTrustManager.checkServerTrusted([LX509, String, SSLEngine) called');
   		console.log('	Arguments: '+authType+', '+engine);
   	};
   	
   	netsectrust.checkServerTrusted.overload('[Ljava.security.cert.X509Certificate;','java.lang.String','java.lang.String').implementation = function (chain, authType, hostname) {
   		console.log('[!] NetworkSecurityTrustManager.checkServerTrusted([LX509, String, String) called');
   		console.log('	Checking: '+authType+' for '+hostname);
   		var X509Certificate = Java.use('[Ljava.security.cert.X509Certificate');
   		return X509Certificate.$new();
   	};
   	
   	netsectrust.checkServerTrusted.overload('[Ljava.security.cert.X509Certificate;', 'java.lang.String').implementation = function (chain, str) {
   		console.log('[!] NetworkSecurityTrustManager.checkServerTrusted([LX509, String) called');
   		console.log('	Arguments: '+str);
   	};
   	
   	netsectrust.checkClientTrusted.overload('[Ljava.security.cert.X509Certificate;','java.lang.String','javax.net.ssl.SSLEngine').implementation = function (chain, authType, engine) {
   		console.log('[!] NetworkSecurityTrustManager.checkClientTrusted([LX509, String, SSLEngine) called');
   		console.log('	Arguments: '+authType+', '+engine);
   	};
   	
   	netsectrust.checkServerTrusted.overload('[Ljava.security.cert.X509Certificate;','java.lang.String','java.net.Socket').implementation = function (chain, authType, engine) {
   		console.log('[!] NetworkSecurityTrustManager.checkServerTrusted([LX509, String, Socket) called');
   		console.log('	Arguments: '+authType+', '+engine);
   	};
   	
   	netsectrust.checkClientTrusted.overload('[Ljava.security.cert.X509Certificate;','java.lang.String','java.net.Socket').implementation = function (chain, authType, engine) {
   		console.log('[!] NetworkSecurityTrustManager.checkClientTrusted([LX509, String, Socket) called');
   		console.log('	Arguments: '+authType+', '+engine);
   	};
	
	console.log('	[+] Android internal NetworkSecurityTrustManager class instances intercepted');
	
	
	/*
	
	org/apache/harmony/xnet/tests/support/X509TrustManagerImpl
	
	org/apache/harmony/xnet/provider/jsse/TrustManagerImpl
   	
   	*/
   	
   	/***************
   	Generic Trust Managers
   	
   	***************/

   	console.log('[+] Injecting code on Generic TrustManagers');
   	
   	var trustman = Java.use('javax.net.ssl.X509ExtendedTrustManager');
   	
   	trustman.checkServerTrusted.overload('[Ljava.security.cert.X509Certificate;','java.lang.String','javax.net.ssl.SSLEngine').implementation = function (chain, authType, engine) {
   		console.log('[!] X509ExtendedTrustManager.checkServerTrusted() called');
   		console.log('	Arguments: '+authType+', '+engine);
   	};
   	
   	trustman.checkClientTrusted.overload('[Ljava.security.cert.X509Certificate;','java.lang.String','javax.net.ssl.SSLEngine').implementation = function (chain, authType, engine) {
   		console.log('[!] X509ExtendedTrustManager.checkServerTrusted() called');
   		console.log('	Arguments: '+authType+', '+engine);
   	};
   	
   	trustman.checkServerTrusted.overload('[Ljava.security.cert.X509Certificate;','java.lang.String','java.net.Socket').implementation = function (chain, authType, engine) {
   		console.log('[!] X509ExtendedTrustManager.checkServerTrusted2() called');
   		console.log('	Arguments: '+authType+', '+engine);
   	};
   	
   	trustman.checkClientTrusted.overload('[Ljava.security.cert.X509Certificate;','java.lang.String','java.net.Socket').implementation = function (chain, authType, engine) {
   		console.log('[!] X509ExtendedTrustManager.checkServerTrusted2() called');
   		console.log('	Arguments: '+authType+', '+engine);
   	};
   	
   	console.log('	[+] Java X509ExtendedTrustManager class instances intercepted');
   	
   	
   	var trustman2 = Java.use('javax.net.ssl.X509TrustManager');
   	
   	trustman2.checkServerTrusted.overload('[Ljava.security.cert.X509Certificate;', 'java.lang.String').implementation = function (chain, authType) {
   		console.log('[!] X509TrustManager.checkServerTrusted() called');
   		console.log('	Arguments: '+authType+', '+engine);
   	};
   	
   	trustman2.checkClientTrusted.overload('[Ljava.security.cert.X509Certificate;', 'java.lang.String').implementation = function (chain, authType) {
   		console.log('[!] X509TrustManager.checkServerTrusted() called');
   		console.log('	Arguments: '+authType+', '+engine);
   	};
   	
   	trustman2.getAcceptedIssuers.implementation = function () {
   	   	console.log('[!] X509TrustManager.getAcceptedIssuers() called');
   		var X509Certificate = Java.use('[Ljava.security.cert.X509Certificate');
   		return X509Certificate.$new();
   	};
   	
   	trustman2.getAcceptedIssuers.implementation = function () {
		console.log('[!] X509TrustManager.getAcceptedIssuers() called');
		return null;
	};   	
   	
   	console.log('	[+] Java X509TrustManager class instances intercepted');

	/***
	/com/sun/net/ssl/internal/ssl/X509ExtendedTrustManager
	***/

	var suntrustman = Java.use('com.sun.net.ssl.internal.ssl.X509ExtendedTrustManager');

	

	suntrustman.checkServerTrusted.overload('[Ljava.security.cert.X509Certificate;', 'java.lang.String', 'java.lang.String', 'java.lang.String').implementation = function (chain, str1, str2, str3) {
		console.log('[!] Sun X509ExtendedTrustManager.checkServerTrusted([LX509, String, String, String) called');
		console.log('	Arguments: '+str1+', '+str2+', '+str3);
	};

	suntrustman.checkClientTrusted.overload('[Ljava.security.cert.X509Certificate;', 'java.lang.String', 'java.lang.String', 'java.lang.String').implementation = function (chain, str1, str2, str3) {
		console.log('[!] Sun X509ExtendedTrustManager.checkClientTrusted([LX509, String, String, String) called');
		console.log('	Arguments: '+str1+', '+str2+', '+str3);
	};
	
	console.log('	[+] Sun internal X509ExtendedTrustManager class instances intercepted');
	

	/*******************
	CertificateFactory
	
	*******************/

   	console.log('[+] Injecting code on SSLCertificateSocketFactory');
   	
   	var sslcertfact = Java.use('android.net.SSLCertificateSocketFactory');
   	
   	sslcertfact.getDefault.overload('int', 'android.net.SSLSessionCache').implementation = function(timeml, cache) {
   		console.log('[!] SSLCertificateSocketFactory.getDefault(int, SSLSessionCache) called');
   		console.log('	returning insecure option');
   		return this.getInsecure(timeml,cache);
   	}
   	
   	sslcertfact.getDefault.overload('int').implementation = function(timeml) {
   		console.log('[!] SSLCertificateSocketFactory.getDefault(int) called');
   		console.log('	returning insecure option');
   		return this.getInsecure(timeml,null);
   	}
   	
   	console.log('	[+] Java SSLCertificateSocketFactory.getDefault() intercepted');   	

    
    /**********
    Javax
    **********/

    
    console.log('[+] Injecting code on javax');
    
    Java.choose('javax.net.ssl.HostnameVerifier', {
    	onMatch: function(instance){
    		console.log('	[+] Found HostnameVerifier class: '+instance.$className);
    		instance.verify.overload('java.lang.String', 'javax.net.ssl.SSLSession').implementation = function (hostname, session) {
				console.log('[!] HostnameVerifier.verify(String, SSLSession) called');
				if (hostname!=null){
					console.log('Checking: '+hostname);
				}
				var	ret = this.verify(hostname, session);
				console.log('	Original verify() call returned: '+ret);
				return true;
			};
    	},
    	onComplete: function () {
    		console.log('	[+] HostnameVerifier.verify() implementations intercepted');
    	}
    });

   
   	/****************
   	SSLContext
   	*****************/
   	
   	console.log('[+] Injecting code on SSLContext');
   	
   	var sslctx = Java.use('javax.net.ssl.SSLContext');
   	
   	sslctx.init.overload('[Ljavax.net.ssl.KeyManager;', '[Ljavax.net.ssl.TrustManager;', 'java.security.SecureRandom').impelementation = function(km, tm, sr) {
   		console.log('[!] SSLContext.init() called');
   		var secrand = Java.use('java.security.SecureRandom');
   		
   		this.init(null, trustman2, secrand);
   		
   	};
   	
   	console.log('	[+] Java SSLContext.init() intercepted');
   	
   	/***********
	okhttp3
	***********/


	
	Java.enumerateLoadedClasses({
    	onMatch: function(className) {
			if (className.includes('okhttp3')) {
			    console.log('[+] Injecting code on okhttp3');
			    console.log('	[+] '+className);
			    
				var okhttp3pinner = Java.use('okhttp3.CertificatePinner');
				
				okhttp3pinner.check.overload('java.lang.String', 'java.util.List').implementation = function (hostname, certs) {
					console.log('[!] okhttp3.CertificatePinner.check(String, List) called');
					if (hostname!=null)
						console.log('Checking: '+hostname);
			
				};
				
				//[L means List<Certificate>
				okhttp3pinner.check.overload('java.lang.String', '[Ljava.security.cert.Certificate;').implementation = function (hostname, certs) {
					console.log('[!] okhttp3.CertificatePinner.check(String, [LCertificate) called');
					if (hostname!=null)
						console.log('Checking: '+hostname);
			
				};
		
				console.log('	[+] okhttp3.CertificatePinner() intercepted');
		
		
				var okhttp3_internal_pinner = Java.use('okhttp3.internal.tls.OkHostnameVerifier');
		
				okhttp3_internal_pinner.verify.overload('java.lang.String', 'javax.net.ssl.SSLSession').implementation = function (hostname, session) {
					console.log('[!] okhttp3.internal.tls.OkHostnameVerifier.verify(String, SSLSession) called');
					if (hostname!=null){
						console.log('Checking: '+hostname);
					}
					var	ret = this.verify(hostname, session);
					console.log('	Original verify() call returned: '+ret);
					return true;
				};
	
				okhttp3_internal_pinner.verify.overload('java.lang.String', 'java.security.cert.X509Certificate').implementation = function (hostname, cert) {
					console.log('[!] okhttp3.internal.tls.OkHostnameVerifier.verify(String, X509) called');
					if (hostname!=null){
						console.log('Checking: '+hostname);
					}
					var	ret = this.verify(hostname, cert);
					console.log('	Original verify() call returned: '+ret);
					return true;
				};
				console.log('	[+] okhttp3.internal.tls.OkHostnameVerifier() intercepted');
			}
		},
		onComplete: function() {}
	
	});
  	
   	
   	console.log('');
});
"""

if len(sys.argv)>1: app=sys.argv[1]
else: app='-h'

if app=='-h':
	help()
	exit()
else:
	process = frida.get_usb_device().attach(app)
	script = process.create_script(jscode)
	script.on('message', on_message)
	print('Launched! Press Ctrl^D to stop')
	print('[*] Running app')
	script.load()
	sys.stdin.read()


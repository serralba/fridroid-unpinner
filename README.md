
# fridroid-unpinner

Script to bypass certificate pinning of Android devices. It hooks pinning functions of Android internals and common libraries, like okhttp3, using frida.

# Dependeces

* [frida](https://github.com/frida/frida/releases)

# Usage

## Run frida on Android

Root access through adb
```
adb root 
```
Move frida-server to phone
```
adb push frida-server /data/local/tmp/ 
```

Make frida executable
```
adb shell "chmod 755 /data/local/tmp/frida-server"
```

Run frida-server
```
adb shell "/data/local/tmp/frida-server &"
```

## Search package

```
adb shell pm list packages | grep PACKAGE_NAME
```

## Run https-unpinner

Unpin HTTPS certificates on that package
```
python https-unpinner.py PACKAGE_NAME
```

# License

This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.

![License image](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)
